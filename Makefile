# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: anmiron <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/27 18:11:43 by anmiron           #+#    #+#              #
#    Updated: 2017/12/20 13:54:06 by pacadis          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit 
FLAGS = -Wall -Wextra -Werror
SRC = *.c
OBJ = $(SRC:.c=.o)
INCL = -I fillit.h -I ./libft/libft.h

all: $(NAME)

$(NAME):
	gcc $(FLAGS) $(INCL) -c $(SRC)
	gcc $(OBJ) -L libft/ -lft -o $(NAME)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
